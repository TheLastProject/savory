def inc(x):
    return x + 1

def test_module():
    assert inc(3) == 4
