#!/bin/bash
rm -rf build/*
rm -rf dist/*
VERSION=$(grep version= setup.py | cut -d "'" -f 2)
python setup.py sdist bdist_wheel
gpg --detach-sign -a "dist/savory-${VERSION}.tar.gz"
twine upload dist/savory-${VERSION}.tar.gz dist/savory-${VERSION}-py3-none-any.whl dist/savory-${VERSION}.tar.gz.asc
